# Alchemy

An open-source (CC-BY) multipurpose crafting system for Godot (3.1 beta 11).

# Main features

See the /docs/docs.html for features and more details.