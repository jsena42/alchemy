"""
Alchemy - Multipurpose crafting system
Author: J. Sena
Version: 1.0
License: CC-BY
URL: https://jsena42.bitbucket.io/alchemy/
Repository: https://bitbucket.org/jsena42/alchemy/
"""

extends Node

##### SETUP #####
## Paths ##
onready var item_scene = load('res://UIItem.tscn')
## Recipes ##
var recipes = [ # First element must be the result.
		['small_dagger', 'iron_ore', 'iron_ore'], # Recipe for 'Small dagger'
		['short_sword', 'iron_ore', 'iron_ore', 'steel_ingot'], # Recipe 'Short sword'
		['long_sword', 'iron_ore', 'iron_ore', 'steel_ingot', 'steel_ingot'], # Recipe for 'Long sword'
		['health_potion', 'green_herb', 'green_herb', 'purple_herb'] # Recipe for 'Health Potion'
		]
## Other customization options ##
var max_slots = 4 # Maximum number of itens used on a recipe
# END OF SETUP #


# Default values. Don't change them unless you really know what you're doing.
var hashed_recipes = [] # Used to check if the combined items match some recipe






func _ready():
	_reset_ui() # Remove all the stuff added for layout preview
	_hash_recipes() # Create ids for matching the selected ingredients with the available recipes
	$Mix.connect('pressed', self, '_on_Mix_pressed') # The mix button
	$Recipebook/HBoxContainer/RecipesList.connect('item_selected', self, '_recipe_selected') # Update the recipebook
	$Slots.connect('mouse_exited', self, '_on_hover_out') # Temporary fix for the hovering label
	_load_inventory() # Load all the items from the PROGRESS singleton.
	_set_recipebook() # Load all the recipes


func _reset_ui():
	$ResultLabel.text = ''
	$Recipebook/HBoxContainer/RecipesIngredients.text = ''
	_clear_inventory()
	_clear_slots()
	$ItemHoverLabel.hide()


func _set_recipebook():
	for recipe in recipes:
		$Recipebook/HBoxContainer/RecipesList.add_item(PROGRESS.items[recipe[0]]['name'])


func _clear_inventory():
	for item in $Inventory.get_children():
		item.queue_free()


func _load_inventory():
	for item in PROGRESS.inventory:
		var current_item = item_scene.instance()
		current_item.ItemName = PROGRESS.items[item]['name']
		current_item.get_node('Icon').frame = PROGRESS.items[item]['icon']
		current_item.ItemId = item
		var amount = PROGRESS.inventory[item]
		if amount < 10: # This is actually optional but I prefer to use the extra zero
			current_item.get_node('Button/Amount').text = '0' + str(amount)
		else:
			current_item.get_node('Button/Amount').text = str(amount)
		$Inventory.add_child(current_item)


func update_inventory():
	_clear_inventory()
	_load_inventory()
	$ItemHoverLabel.hide_label()


func _clear_slots():
	for item in $Slots.get_children():
		item.queue_free()
	


func add_to_slots(id):
	var slots_count = $Slots.get_child_count()
	if slots_count < max_slots: # Check if can add another item to the mix
		var current_item = item_scene.instance()
		current_item.ItemName = PROGRESS.items[id]['name']
		current_item.get_node('Icon').frame = PROGRESS.items[id]['icon'] # This only works if all the icons are on the same image
		current_item.ItemId = id
		current_item.OnSlot = true
		current_item.get_node('Button/Amount').hide() # The amount on the slots is always 1 so this we'll hide this
		$Slots.add_child(current_item)
		$ItemHoverLabel.hide_label()
	else: # All slots are full. Don't add another item to the mix
		pass


func _hash_recipes():
	for recipe in recipes: # Loop through the array
		var new_recipe = recipe.duplicate() # Create a new subarray to preserve the orignal 
		new_recipe.pop_front() # Remove the first element (the resulting item)
		new_recipe.sort() # Sort it alphabetically
		hashed_recipes.append(new_recipe.hash()) # Hash the resulting array and add it to the hastable (the hashed_recipes array)


func _match_recipe(value):
	value.sort() # Sort it aplhabetically
	var resulting_item = hashed_recipes.find(value.hash()) # Checks the hashed_recipes array for the current mix
	if resulting_item >= 0: # The mix matches a recipe
		_clear_slots() # Remove the items on the slots
		$ResultLabel.text = PROGRESS.items[recipes[resulting_item][0]]['name'] # Shows the resulting item's name
		if PROGRESS.inventory.has(recipes[resulting_item][0]): # Check if already have one or more of the item
			PROGRESS.inventory[recipes[resulting_item][0]] += 1 # Already have so add one more
		else:
			PROGRESS.inventory[recipes[resulting_item][0]] = 1 # Doesn't have so add one
	update_inventory() # Redraws the inventory


func _on_Mix_pressed():
	$ResultLabel.text = '' # Clean the label
	var array = [] # Create an array to pass as an argument in the _match_recipe() function
	for item in $Slots.get_children(): # Loop through the current items on the slots
		array.append(item.ItemId) # Add the item's id to the array  
	
	_match_recipe(array)


func _recipe_selected(recipe): # Show the ingredients of the recipe
	$Recipebook/HBoxContainer/RecipesIngredients.text = '' # Resets the label
	for ingredient in range(1, recipes[recipe].size()): # Loop through the recipe array skipping the first item (the resulting one)
		$Recipebook/HBoxContainer/RecipesIngredients.text += PROGRESS.items[recipes[recipe][ingredient]]['name'] + '\n' 


func _on_hover_out():
	$ItemHoverLabel.hide_label()