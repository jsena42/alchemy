extends Node

var items = {
	'iron_ore' : { 'name' : 'Iron Ore', 'icon': 0 },
	'steel_ingot' : { 'name' : 'Steel Ingot', 'icon': 1 },
	'green_herb' : { 'name' : 'Green Herb', 'icon': 2 },
	'yellow_herb' : { 'name' : 'Yellow Herb', 'icon': 3 },
	'purple_herb' : { 'name' : 'Purple Herb', 'icon': 4 },
	'dragon_blood' : { 'name' : 'Dragon Blood', 'icon': 5 },
	'dragon_scales' : { 'name' : 'Dragon Scales', 'icon': 6 },
	'fire_stone' : { 'name' : 'Fire Stone', 'icon': 7 },
	'small_dagger' : { 'name' : 'Small Dagger', 'icon': 8 },
	'short_sword' : { 'name' : 'Short Sword', 'icon': 9 },
	'long_sword' : { 'name' : 'Long Sword', 'icon': 10 },
	'health_potion' : { 'name' : 'Health Potion', 'icon': 11 }
}

var inventory = {
	'iron_ore' : 7,
	'steel_ingot' : 3,
	'green_herb' : 4,
	'yellow_herb' : 3,
	'purple_herb' : 2,
	'dragon_blood' : 1,
	'dragon_scales' : 3,
	'fire_stone' : 1
}


func add_item(item, value):
	if inventory.has(item):
		inventory[item] += value
	else:
		inventory[item] = value