extends Container

var ItemName = ''
var ItemId = ''
var OnSlot = false

onready var Alchemy = get_node('/root/Alchemy')
onready var ItemHoverLabel = get_node('/root/Alchemy/ItemHoverLabel')
onready var Slots = get_node('/root/Alchemy/Slots')
onready var Inventory = get_node('/root/Alchemy/Inventory')






func _ready():
	$Button.connect('mouse_entered', self, '_on_hover_in')
	$Button.connect('mouse_exited', self, '_on_hover_out')
	$Button.connect('pressed', self, '_on_click')


func _remove_from_inventory():
	if PROGRESS.inventory[ItemId] > 1:
		PROGRESS.inventory[ItemId] -= 1
		var amount = PROGRESS.inventory[ItemId]
		if amount < 10:
			$Button/Amount.text = '0' + str(amount)
		else:
			$Button/Amount.text = str(amount)
	else:
		PROGRESS.inventory.erase(ItemId)
		self.queue_free()


func _add_to_inventory():
	if PROGRESS.inventory.has(ItemId):
		PROGRESS.inventory[ItemId] += 1
	else:
		PROGRESS.inventory[ItemId] = 1
	Alchemy.update_inventory()
	ItemHoverLabel.hide_label()
	self.queue_free()
	

func _on_hover_in():
	ItemHoverLabel.show_label()
	ItemHoverLabel.text = ItemName


func _on_hover_out():
	ItemHoverLabel.hide_label()


func _on_click():
	if OnSlot:
		_add_to_inventory()
	else:
		var slots_count = Slots.get_child_count()
		if slots_count < Alchemy.max_slots:
			_remove_from_inventory()
		Alchemy.add_to_slots(ItemId)